<?php

/**
 * Config of watched wikisites
 * @ingroup Extensions
 * @author Josef Martiňák
 */

# array(URL,stat's filename,custom stat's wiki page)
$wikis[0] = array("https://www.wikiskripta.eu","WikiSkripta.csv","WikiSkripta:Statistiky");
$wikis[1] = array("https://www.wikilectures.eu","WikiLectures.csv","WikiLectures:Statistics");
$wikis[2] = array("https://www.statest.cz","StaTest.csv","");
$wikis[3] = array("https://wikibank.lf1.cuni.cz","ItemBankWiki.csv","");

?>